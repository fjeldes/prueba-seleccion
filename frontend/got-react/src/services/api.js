import store from '../config/store';
import { API_CONFIG } from '../config/configurations';

const url = () => API_CONFIG.url;
const globalTimeout = API_CONFIG.timeout;
const timeoutMessage =
  'Está tardando demasiado, verifica tu conexión a internet e intenta nuevamente';

export default class API {
  static getGlobalTimeout() {
    return globalTimeout;
  }

  static getTimeoutMessage() {
    return timeoutMessage;
  }

  static genericErrorMessage(status) {
    return status === 404 ? 'Recurso no encontrado' : 'Intentelo más tarde';
  }

  static get(route) {
    console.log("route "+url()+route);
    
    return fetch(url() + route, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  }

  static postLogin(route, params = {}) {
    return fetch(url() + route, {
      method: 'POST',
      mode: 'CORS',
      cache: 'no-cache',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'access-token': store.getState().auth.headers.accessToken,
        client: store.getState().auth.headers.client,
        uid: store.getState().auth.headers.uid,
      },
      body: JSON.stringify(params),
    });
  }

  static post(route, params = {}) {
    const token = localStorage.getItem('token');
    console.log("Bearer "+token);
    
    return fetch(url() + route, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(params),
    }
    );

    
  }

  static delete(route) {
    return fetch(url() + route, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'access-token': store.getState().auth.headers.accessToken,
        client: store.getState().auth.headers.client,
        uid: store.getState().auth.headers.uid,
      },
    });
  }
}
