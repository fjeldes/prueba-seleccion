import { takeEvery, put} from 'redux-saga/effects';
import { runDefaultSaga } from './default';

import API from '../services/api';

import {

    GET_CHARACTERS,
    GET_CHARACTERS_SUCCESS,
    GET_CHARACTERS_FAILURE,

    GET_CHARACTER,
    GET_CHARACTER_SUCCESS,
    GET_CHARACTER_FAILURE,

} from '../actions/characters';



const getCharactersRequest = () => API.get('characters');


function* getCharactersRequestSuccessCallback(result) {
 
    
  yield put({
    type: GET_CHARACTERS_SUCCESS,
    characters: result,
  });
}
function* getCharactersRequestFailureCallback() {

  yield put({
    type: GET_CHARACTERS_FAILURE,
  });
}

function* getCharacters(action) {

    
    yield runDefaultSaga(
      { request: getCharactersRequest },
      getCharactersRequestSuccessCallback,
      getCharactersRequestFailureCallback
    );
  }


  /////////

  const getCharacterRequest = id => {

     return API.get('characters/'+id);
  } ;


function* getCharacterRequestSuccessCallback(result) {

  yield put({
    type: GET_CHARACTER_SUCCESS,
    characters: result,
  });
}
function* getCharacterRequestFailureCallback() {

  yield put({
    type: GET_CHARACTER_FAILURE,
  });
}

function* getCharacter(action) {

    
    yield runDefaultSaga(
      { request: getCharacterRequest, params: action.params},
      getCharacterRequestSuccessCallback,
      getCharacterRequestFailureCallback
    );
  }

 

export default function* characterSaga() {
  yield takeEvery(GET_CHARACTERS, getCharacters);
  yield takeEvery(GET_CHARACTER, getCharacter);
}
