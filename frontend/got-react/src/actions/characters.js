export const GET_CHARACTERS = 'GET_CHARACTERS';
export const GET_CHARACTERS_SUCCESS = 'GET_CHARACTERS_SUCCESS';
export const GET_CHARACTERS_FAILURE = 'GET_CHARACTERS_FAILURE';

export const GET_CHARACTER = 'GET_CHARACTER';
export const GET_CHARACTER_SUCCESS = 'GET_CHARACTER_SUCCESS';
export const GET_CHARACTER_FAILURE = 'GET_CHARACTER_FAILURE';


export const getCharacters = () => ({ type: GET_CHARACTERS});
export const getCharacter = params => ({ type: GET_CHARACTER, params});
