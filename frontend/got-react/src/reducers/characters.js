import {
    GET_CHARACTERS,
    GET_CHARACTERS_SUCCESS,
    GET_CHARACTERS_FAILURE,

    GET_CHARACTER,
    GET_CHARACTER_SUCCESS,
    GET_CHARACTER_FAILURE,

  } from '../actions/characters';
  
  const initialState = {
 
    characters: [],
    character: [],
    characterTitles: [],
    
    ongoingRequest: {
        characters: false,
     
    },

    ongoingRequestCharacter: {
      character: false,
   
  },
    numeroRegistros: 0,

  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case GET_CHARACTERS:
        console.log(action);
        
        return {
          ...state,
          ongoingRequest: { ...state.ongoingRequest, characters: false},
        };
      case GET_CHARACTERS_SUCCESS:
        console.log(action);
       
        return {
          ...state,
          characters: action.characters.body,
          
          numeroRegistros: action.characters.totalRegistros,
          ongoingRequest: { ...state.ongoingRequest, characters: true },
        };
      case GET_CHARACTERS_FAILURE: {
        console.log(action);
        return {
          ...state,
          ongoingRequest: { ...state.ongoingRequest, characters: false },
        };
      }

      case GET_CHARACTER:
        console.log(action);
        
        return {
          ...state,
          ongoingRequestCharacter: { ...state.ongoingRequestCharacter, character: false},
        };
      case GET_CHARACTER_SUCCESS:
        console.log(action);
       
        return {
          ...state,
          character: action.characters.body,
          characterTitles: action.characters.body2,
          ongoingRequestCharacter: { ...state.ongoingRequestCharacter, character: true },
        };
      case GET_CHARACTER_FAILURE: {
        console.log(action);
        return {
          ...state,
          ongoingRequestCharacter: { ...state.ongoingRequestCharacter, character: false },
        };
      }

     
      default:
        return { ...state };
    }
  };
  
  export default reducer;
  