import React, { Component } from "react";
import { connect } from 'react-redux';
import { getCharacters } from '../actions/characters';


// Components
import { BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

import CharacterDetail from './CharacterDetail';

//Assets

import './Home.css';


class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
        visibleDetailCharacter : true,
    };

   
 
    this.clickOpenDetailCharacter = this.clickOpenDetailCharacter.bind(this);
    this.clickCloseDetailCharacter = this.clickCloseDetailCharacter.bind(this);
 
    
  }

  componentDidMount() {


    this.props.dispatch(getCharacters());
 
  }

  clickOpenDetailCharacter(e) {

    this.props.characters.map((character,i) => {
      
        if(character.idCharacter === e.target.parentElement.id){
    
            this.setState ({
                  visibleDetailCharacter:false,
                  haracterActual : character,})
    
          }
      });
            
    }

  clickCloseDetailCharacter() {
      
    this.setState ({
        visibleDetailCharacter:true})
  }

  onSelectCharacter = (row) => {
           
    this.setState ({
            visibleDetailCharacter:false,
            characterActual : row,})
          };


  render() {

    const { visibleDetailCharacter, } = this.state;

    const selectRow = {
        mode: 'radio',
        clickToSelect: true,
        onSelect: this.onSelectCharacter
      };
    
    return (
      <div className="App">
        <div className="card">
                  
              
            {this.props.characters ?    
                    
              <BootstrapTable  data={this.props.characters} selectRow={ selectRow } pagination={true} striped hover>
                  <TableHeaderColumn thStyle={{width: '20%'}} tdStyle={{width: '20%'}} isKey dataField='idCharacter'>#</TableHeaderColumn>
                  <TableHeaderColumn thStyle={{width: '35%'}} tdStyle={{width: '35%'}} filter={ { type: 'TextFilter', delay: 200 } } dataField='nameCharacter'>Name</TableHeaderColumn>
                  <TableHeaderColumn thStyle={{width: '10%'}} tdStyle={{width: '10%'}} dataField='genderCharacter'>Gender</TableHeaderColumn>
                  <TableHeaderColumn thStyle={{width: '35%'}} tdStyle={{width: '35%'}} filter={ { type: 'TextFilter', delay: 200 } } dataField='houseCharacter'>House</TableHeaderColumn>
                 
              </BootstrapTable>: '' }
 
         </div>

        {visibleDetailCharacter?null: <CharacterDetail characterActual={this.state.characterActual} display={this.state.visibleDetailCharacter} onClick={this.clickCloseDetailCharacter}/>}
      </div>
    );
  }
}



const mapStateToProps = state => {
    //console.log(state);
    const { characters, ongoingRequest, numeroRegistros} = state.characters;
    return {
      characters,
      ongoingRequest,
      numeroRegistros,
    };
  };




export default connect(mapStateToProps)(Home);
