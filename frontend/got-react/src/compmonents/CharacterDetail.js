import React, { Component } from 'react';

import { connect } from 'react-redux';
import { getCharacter } from '../actions/characters';


//Assets
import './Home.css';

class CharacterDetail extends Component{

  constructor(props){
    super(props);

    this.state = {

        characterActual:this.props.characterActual,
      
    }

    this.getTitulos = this.getTitulos.bind(this);
    this.getLibros = this.getLibros.bind(this);
      
  }

  componentDidMount(){
    
    this.props.dispatch(getCharacter(this.props.characterActual.idCharacter));
  }

  getLibros = () => {
 
    return this.props.character.map((book,i) => {

        return (
            <p className="p-data">{i+1}. {book.bookCharacter}</p>
       
        );

    })

    }

    getTitulos = () => {

        return this.props.characterTitles.map((title,i) => {

     
            return (
                <p className="p-data">{i+1}. {title.tituloCharacter}</p>
       
            );

         })

    }

    render(){
        
        return (
            
            <div className="row mt-5 mb-5 box-character">
                <div className="col-md-8 center-box">
                    <div className="card conductor box-pop-character">
                  
                        <div className="card-header">

                                    <h3>{this.state.characterActual.nameCharacter}</h3>
                            
                                    <button type="button " className="close btn-close" aria-label="Close" onClick={this.props.onClick}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                    
                        </div>

                        <div className="row card-body box-data">

                            <div className="col-md-12">

                                <div className="row">

                                    <div className="col-md-4">

                                        <img className="img-perfil" src={this.state.characterActual.imageCharacter}></img>

                                    </div>

                                    <div className="col-md-2">
                                    
                                        <p className="p-data"> <i className="fas fa-envelope"> </i>Slug:</p>
                                        <p className="p-data">{this.state.characterActual.slugCharacter}</p>
                                        <p className="p-data"> <i className="fas fa-key"></i> Gender:</p>
                                        <p className="p-data">{this.state.characterActual.genderCharacter}</p>
                                        <p className="p-data"> <i className="fas fa-mobile-alt"> </i>Culture:</p>
                                        <p className="p-data">{this.state.characterActual.cultureCharacter}</p>
                                        <p className="p-data"> <i className="far fa-id-card"> </i>House:</p>
                                        <p className="p-data">{this.state.characterActual.houseCharacter}</p>
                                        <p className="p-data"> Rank:</p>
                                        <p className="p-data">{this.state.characterActual.rankCharacter}</p>
                                    </div>

                                    <div className="col-md-2">
                                    
                                    <p className="p-data"> <i className="fas fa-envelope"> </i>Libros:</p>
                                    {this.props.character ?  this.getLibros(): '' }

                                
                    
                                     </div>

                                     <div className="col-md-2">
                                    
                                    <p className="p-data"> <i className="fas fa-envelope"> </i>Títulos:</p>
                                    {this.props.character ?  this.getTitulos(): '' }
                    
                                </div>
                                </div>
                            </div>

                           
                        </div>

                        <div >


                        </div>

                    
                    </div>
            
                </div>

            </div>

            
        )

    }

}

const mapStateToProps = state => {
    //console.log(state);
    const { character,characterTitles, ongoingRequestCharacter} = state.characters;
    return {
      character,
      characterTitles,
      ongoingRequestCharacter,
    };
  };

export default connect(mapStateToProps)(CharacterDetail);