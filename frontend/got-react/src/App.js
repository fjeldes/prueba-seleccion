import React, { Component } from 'react';
import { Route, Switch} from 'react-router-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import './App.css';
import { Provider } from 'react-redux';
import store from './config/store';

import Home from './compmonents/Home';


class App extends Component {


  render(){
  return (

    <div className="cont">

      <Provider store={store}>
        <Router>
          <Switch>
              <Route path="/" exact component={Home}></Route>
          
          </Switch>
        </Router>
      </Provider>

    </div>
   
     
       
     
   
  );
  }
}



export default App;
