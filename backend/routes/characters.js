const express = require('express');
const router = express.Router();
const characterController = require('../controllers/charactersController');


router.get('/',characterController.coorsPermit,characterController.getCharacters);
router.get('/:id',characterController.coorsPermit,characterController.getCharacter);

module.exports = router;