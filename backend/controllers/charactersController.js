const controller = {};
const mysql = require('mysql');

var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password:'',
    database: 'bd_got',
    dateStrings: true,
    port:'3306'
 
  });

controller.getCharacters = (req,res) =>{
    console.log("Characters");

    req.getConnection((err,conn)=>{
      const query = ' SELECT COUNT(*) AS total FROM personajes';
    
          conn.query(query,(err,numPersonajes)=>{
         
              if(err){
                            
                 respuesta = {'status':false,
                  'mensaje': err};
                   res.json(respuesta); 
               }else{
              
                req.getConnection((err,conn)=>{
                  const query = ' SELECT *FROM personajes ORDER BY nameCharacter';
                
                      conn.query(query,(err,personajes)=>{
                     
                          if(err){
                                        
                             respuesta = {'status':false,
                              'mensaje': err};

                            res.json(respuesta); 
                           }else{
                           
                              respuesta = {'status':true,
                              'mensaje':'Personajes obtenidos con exito',
                              'body':personajes,
                              'totalRegistros': numPersonajes[0].total};

                              //console.log(respuesta);
                              
                              res.json(respuesta); 
                           }
                   
                         });
                 
                  });
            
               
               }
       
             });
     
      });
  };

  controller.getCharacter = (req,res) =>{
   
    console.log(req.params.id);
    
    req.getConnection((err,conn)=>{
    
        const query = 'SELECT DISTINCT l.bookCharacter FROM personajes p, libros l, libros WHERE p.idCharacter = "'+req.params.id+'" AND p.idCharacter = l.idCharacter';
                
        conn.query(query,(err,personaje)=>{
                     
            if(err){
                                        
                respuesta = {'status':false,
                              'mensaje': err};

                res.json(respuesta); 

            }else{
                           
                respuesta = {'status':true,
                            'mensaje':'Personaje obtenido con éxito',
                            'body':personaje};

                            const queryTitulos = 'SELECT DISTINCT t.tituloCharacter FROM personajes p, titulos t, libros WHERE p.idCharacter = "'+req.params.id+'" AND p.idCharacter = t.idCharacter';
                
                            conn.query(queryTitulos,(err,titulo)=>{
                                         
                                if(err){
                                                            
                                    respuesta = {'status':false,
                                                  'mensaje': err};
                    
                                    res.json(respuesta); 
                    
                                }else{
                                               
                                    respuesta = {'status':true,
                                                'mensaje':'Personaje obtenido con éxito',
                                                'body':personaje,
                                                'body2': titulo};
                    
                                              
                                    res.json(respuesta); 
                    
                                    }
                                       
                            });

                }
                   
        });
                 
    });
        
     
  };

  
  controller.coorsPermit = (req, res, next) =>{
    console.log("ddd");
    
     res.header('Access-Control-Allow-Origin', '*');
     res.header('Access-Control-Allow-Credentials', true);
     res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
     res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
     next();
 
 }
  module.exports = controller;