const express = require('express');
const app = express();
const mysql = require('mysql');
const myConnection = require('express-myconnection');
const cors = require('cors');
const request = require("request-promise"), RUTA = "https://api.got.show/api/book/characters";

const charactersRouter = require('./routes/characters');

app.use(cors());

app.use(myConnection(mysql,{
    host: '127.0.0.1',
     user: 'root',
     password:'',
     database: 'bd_got',
     dateStrings: true,
     port:'3306'
  
  },'single'));
  
  
  var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password:'',
    database: 'bd_got',
    dateStrings: true,
    port:'3306'
 
  });


app.use('/characters', charactersRouter);



app.get('/', function (req, res) {

  res.send('Hola Mundo!');

});

app.listen(3000, function () {
     console.log('Example app listening on port 3000!');
    const query = 'INSERT INTO personajes (idCharacter, nameCharacter, slugCharacter, genderCharacter, cultureCharacter, houseCharacter, aliveCharacter, imageCharacter, rankCharacter) VALUES (?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE nameCharacter = VALUES(nameCharacter), slugCharacter = VALUES(slugCharacter), genderCharacter = VALUES(genderCharacter), cultureCharacter = VALUES(cultureCharacter), houseCharacter = VALUES(houseCharacter), aliveCharacter = VALUES(aliveCharacter), imageCharacter = VALUES(imageCharacter), rankCharacter = VALUES(rankCharacter)';
    const queryBooks = 'INSERT INTO libros (idCharacter, bookCharacter) VALUES (?,?) ON DUPLICATE KEY UPDATE idCharacter = VALUES(idCharacter), bookCharacter = VALUES(bookCharacter)';
    const queryTitulos = 'INSERT INTO titulos (idCharacter, tituloCharacter) VALUES (?,?) ON DUPLICATE KEY UPDATE idCharacter = VALUES(idCharacter), tituloCharacter = VALUES(tituloCharacter)';
 
    var booksAux = [];
    var titulosAux = [];

    request({
        uri: RUTA,
        json: true, 
    }).then(characters => {

        //console.log("Tamano"+characters.length);
        
        characters.forEach(character => {
            //console.log(`Tenemos a un personaje llamado ${character.name}`);
           // console.log(character.books.length);
            
            for(let i = 0; i< character.books.length ; i++){
                booksAux.push({idCharacter: character.id, nombre: character.books[i]})
            }

            for(let i = 0; i< character.titles.length ; i++){
                titulosAux.push({idCharacter: character.id, titulo: character.titles[i]})
            }
            
            connection.query(query,[character.id, character.name, character.slug, character.gender, character.culture, character.house, character.alive, character.image, character.pagerank ? character.pagerank.rank : null],(err,character)=>{

                if(err){
            
                
                //console.log(err.sqlMessage);
            
                }else{
                    
                    
                
               
                }
            });
        })

        booksAux.map((book,i)=>{

          //  console.log(book);
            
            connection.query(queryBooks,[book.idCharacter, book.nombre],(err,character)=>{

                if(err){
            
                
                //console.log(err.sqlMessage);
            
                }else{
                    
                    
                
               
                }
            });
            
        })

        titulosAux.map((titulo,i)=>{

            console.log(titulo);
            
            connection.query(queryTitulos,[titulo.idCharacter, titulo.titulo],(err,character)=>{

                if(err){
            
                
                //console.log(err.sqlMessage);
            
                }else{
                    
                    
                
               
                }
            });
            
        })
    });

});

module.exports = app;